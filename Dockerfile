ARG PROJECT_NAME_DEFAULT=hellogcp
FROM theopencompany/alpine-cpp:latest as builder
ARG PROJECT_NAME_DEFAULT
ARG BUILD_MODE=Docker
ARG BUILD_TYPE=Release
ENV PROJECT_NAME $PROJECT_NAME_DEFAULT

WORKDIR /
VOLUME ["/project"]
WORKDIR ["/project"]

# Copy local code to the container image.
WORKDIR ${PROJECT_NAME}
COPY . .
RUN rm -rf build
RUN mkdir build
RUN cd build && cmake -DAPP_NAME=${PROJECT_NAME}  -DBUILD_MODE=${BUILD_MODE} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} .. && make && sudo make install

# Copy the binary to the production image from the builder stage.
FROM alpine:latest
ARG PROJECT_NAME_DEFAULT
ENV PROJECT_NAME $PROJECT_NAME_DEFAULT
RUN apk add --no-cache ca-certificates

COPY --from=builder /usr/local/bin/${PROJECT_NAME} /usr/local/bin/${PROJECT_NAME}
RUN chmod +x /usr/local/bin/${PROJECT_NAME}

ENV PORT=8080
EXPOSE 8080

# Run the web service on container startup.
CMD $PROJECT_NAME
