# HELLOGCP

THIS IS A EXAMPLE TO BUILD C++ PROJECT WITH GRPC PROTOBUF MYSQL IN SINGLE STATIC BINARY MADE TO WORK IN SMALL IMAGE OF DOCKER ALPINE FOR
VM, GOOGLE CLOUD RUN, GOOGLE CLOUD ENGINE AND ALL OTHER CLOUD SERVICES THAT SUPPORT DOCKER

# about the dockerfile
This docker file use other docker file called alpine-cpp as base that contains all the needed libs: 
- c/c++
- mysql-cpp-connector 8.x with jdbc support
- pistache for restservices
- protobuf
- grpc

so every time that you need to create a new project you only need to use alpine-cpp image and build on it adding only the
particular things for your project.

# what do this docker file?

- use alpine-cpp as base
- build your project in the docker base
- copy your final binary to new minimal docker alpine 

# what you need to setup in this docker file:
you need to set the arg:
- PROJECT_NAME_DEFAULT : is the app name of the final executable binary of your project 
- BUILD_MODE : can be Debug or Release its use different optimizations flags 
- BUILD_TYPE : Docker|Local if you want to build for docker or locally, on Docker the final executable is static and in local is shared


you can set it inside the dockerfile or when you build it example:

docker build --build-arg PROJECT_NAME_DEFAULT=myappname --build-arg BUILD_TYPE=Docker --build-arg BUILD_MODE=Release

# About the c++ project

This projects is made to work as base and example project to create a serverless backend app in google cloud run or also run in docker container
the idea is to have the common folder as a git submodule so when you need to create a new app you dont need to set again all the cmake config and also you can have common proto models for all your backend apps and when you mod one model all others microservices have that mod too
it is tested and working on linux and macosx the requeriments to run it local are:

- commons c++ standar libs
- cmake >= 3.10 
- mysqlconnector c++ 8.0.16
- protobuf 3.8.x
- grpc 1.21.x

how to build local:

mkdir build
cd build
cmake -DAPP_NAME=${PROJECT_NAME}  -DBUILD_MODE=${BUILD_MODE} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} .. && make && sudo make install